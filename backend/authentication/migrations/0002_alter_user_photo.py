# Generated by Django 4.0.5 on 2022-06-27 20:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='photo',
            field=models.ImageField(default='users/photos/default.png', upload_to='users/photos', verbose_name='Фото'),
        ),
    ]
