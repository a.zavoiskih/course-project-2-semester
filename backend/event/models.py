from django.db import models

from authentication.models import User


class Event(models.Model):
    title = models.CharField(verbose_name='Название', max_length=255)
    description = models.TextField(verbose_name='Описание')
    type = models.CharField(verbose_name='Тип', max_length=255)
    begins_at = models.DateTimeField(verbose_name='Начинается')
    ends_at = models.DateTimeField(verbose_name='Заканчивается')
    place = models.CharField(verbose_name='Место', max_length=255)
    cover = models.ImageField(verbose_name='Обложка', upload_to='events')
    price = models.IntegerField(verbose_name='Цена')
    min_age = models.IntegerField(verbose_name='Нижнее ограничение по возрасту')
    vacancies_amount = models.IntegerField(verbose_name='Количество свободных мест')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Событие'
        verbose_name_plural = 'События'


class FavoriteEvent(models.Model):
    user = models.ForeignKey(User, verbose_name='User', on_delete=models.CASCADE)
    event = models.ForeignKey(Event, verbose_name='Event', on_delete=models.CASCADE)

    def __str__(self):
        return self.user.email + ' ' + self.event.title

    class Meta:
        verbose_name = 'Избранное'
        verbose_name_plural = 'Избранные'


class Hall(models.Model):
    event = models.ForeignKey(Event, verbose_name='Событие', on_delete=models.CASCADE)
    title = models.CharField(verbose_name='Название', max_length=255)
    description = models.TextField(verbose_name='Описание')
    rows_amount = models.IntegerField(verbose_name='Количество рядов')
    places_amount = models.IntegerField(verbose_name='Количество мест в ряду')

    def __str__(self):
        return self.event.title + '. ' + self.title

    class Meta:
        verbose_name = 'Зал'
        verbose_name_plural = 'Залы'


class Place(models.Model):
    hall = models.ForeignKey(Hall, verbose_name='Зал', on_delete=models.CASCADE)
    row = models.IntegerField(verbose_name='Ряд')
    place = models.IntegerField(verbose_name='Место')

    def __str__(self):
        return f"Событие: {self.hall.event.title}. Зал: {self.hall.title}. Ряд: {self.row}. Место: {self.place}"

    class Meta:
        verbose_name = 'Место'
        verbose_name_plural = 'Места'


class Ticket(models.Model):
    user = models.ForeignKey(User, verbose_name='User', on_delete=models.CASCADE)
    event = models.ForeignKey(Event, verbose_name='Event', on_delete=models.CASCADE)
    count = models.IntegerField(verbose_name='Куплено')

    def __str__(self):
        return self.user.email + ': ' + self.event.title

    class Meta:
        verbose_name = 'Билет'
        verbose_name_plural = 'Билеты'

