from django.db.models import Sum
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from event.serializers import EventSerializer, FavoriteEventSerializer, TicketSerializer
from event.models import Event, FavoriteEvent, Ticket
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated


class EventViewSet(ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    @action(methods=['POST'], detail=True, permission_classes=[IsAuthenticated], url_path='toggle-favorite')
    def toggle_favorite(self, request, pk=None):
        user = request.user
        try:
            event = self.queryset.get(pk=pk)
        except Event.DoesNotExist:
            raise NotFound('event not found')
        try:
            fav_event = FavoriteEvent.objects.get(user=user, event=event)
            fav_event.delete()
            return Response({'message': 'removed'})
        except FavoriteEvent.DoesNotExist:
            FavoriteEvent.objects.create(user=user, event=event)
            return Response({'message': 'added'})

    @action(methods=['GET'], detail=False, permission_classes=[IsAuthenticated], url_path='favorites')
    def get_favorites(self, request):
        user = request.user
        events = FavoriteEvent.objects.filter(user=user)
        data = FavoriteEventSerializer(instance=events, many=True).data
        return Response(data)

    @action(methods=['GET'], detail=False, permission_classes=[IsAuthenticated], url_path='tickets')
    def get_tickets(self, request):
        user = request.user
        events = Ticket.objects.all() if user.is_superuser else Ticket.objects.filter(user=user)
        data = TicketSerializer(instance=events, many=True).data
        return Response({"tickets": data, "isAdmin": user.is_superuser, **events.aggregate(Sum('count'))})

    @action(methods=['DELETE'], detail=True, permission_classes=[IsAuthenticated], url_path='tickets')
    def delete_ticket(self, request, pk=None):
        if not request.user.is_superuser:
            return Response({'message': 'not authorized to do that'})
        try:
            ticket = Ticket.objects.get(pk=pk)
            event = Event.objects.get(pk=ticket.event_id)
            event.vacancies_amount += ticket.count
            event.save()
            ticket.delete()
            return Response({'message': 'removed'})
        except FavoriteEvent.DoesNotExist:
            raise NotFound('ticket not found')

    @action(methods=['PUT'], detail=True, permission_classes=[IsAuthenticated], url_path='buy-ticket')
    def buy_ticket(self, request, pk=None):
        user = request.user
        try:
            event = self.queryset.get(pk=pk)
        except Event.DoesNotExist:
            raise NotFound('event not found')
        print(request.data.get('tickets_amount'))
        tickets_amount = int(request.data.get('tickets_amount'))
        if tickets_amount > event.vacancies_amount:
            return Response({'message': 'Количество покупаемых билетов превосходит количество свободных мест, '
                                        'обновите страницу'})
        event.vacancies_amount -= tickets_amount
        event.save()
        try:
            ticket = Ticket.objects.get(user=user, event=event)
            ticket.count += tickets_amount
            ticket.save()
            return Response({'message': 'added'})
        except Ticket.DoesNotExist:
            Ticket.objects.create(user=user, event=event, count=tickets_amount)
            return Response({'message': 'added'})
