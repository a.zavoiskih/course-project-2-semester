from rest_framework import serializers
from event.models import Event, FavoriteEvent, Ticket
from authentication.serializers import UserSerializer


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = '__all__'


class FavoriteEventSerializer(serializers.ModelSerializer):
    user_data = UserSerializer(source='user')
    event_data = EventSerializer(source='event')

    class Meta:
        model = FavoriteEvent
        exclude = ['user', 'event']


class TicketSerializer(serializers.ModelSerializer):
    user_data = UserSerializer(source='user')
    event_data = EventSerializer(source='event')

    class Meta:
        model = Ticket
        exclude = ['user', 'event']
