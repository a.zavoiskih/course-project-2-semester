from django.contrib import admin
from event.models import Event, Place, Hall
from event.models import Ticket
from event.models import FavoriteEvent


class EventAdmin(admin.ModelAdmin):
    list_filter = ('type', )
    search_fields = ('title', 'description')


class TicketAdmin(admin.ModelAdmin):
    list_filter = ('event__type', )
    search_fields = ('event__title', 'event__description')


admin.site.register(Event, EventAdmin)
admin.site.register(Ticket, TicketAdmin)
admin.site.register(FavoriteEvent)
admin.site.register(Hall)
admin.site.register(Place)
