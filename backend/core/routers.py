from rest_framework.routers import DefaultRouter

from authentication.views import UserViewSet
from event.views import EventViewSet

router = DefaultRouter()

router.register('events', EventViewSet)
router.register('auth', UserViewSet)
